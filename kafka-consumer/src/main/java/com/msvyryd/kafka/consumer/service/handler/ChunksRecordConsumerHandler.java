package com.msvyryd.kafka.consumer.service.handler;

import com.msvyryd.kafka.common.KafkaEnvVars;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;

import static com.msvyryd.kafka.common.CommonUtils.*;

public class ChunksRecordConsumerHandler implements RecordConsumerHandler<byte[]> {
    private static final Logger LOG = LoggerFactory.getLogger(COSRecordConsumerHandler.class);

    private static final int INVALID = -1;

    private ByteArrayOutputStream baos;
    private String topic;

    public ChunksRecordConsumerHandler() {
        this.baos = new ByteArrayOutputStream();
        this.topic = getEnv(KafkaEnvVars.TOPIC_CHUNKS);
    }

    @Override
    public void consume(ConsumerRecord<String, byte[]> record) {
        // get current chunk
        int currentChunk = resolveInt(record.headers(), CURRENT_CHUNK);
        // get total chunks
        int totalChunks = resolveInt(record.headers(), TOTAL_CHUNKS);

        // add chunk bytes
        try {
            baos.write(record.value());
        } catch (IOException e) {
            LOG.warn("Unable to write chunk!", e);
        }

        // check if current chunk is last
        if (currentChunk == totalChunks) {
            byte[] content = baos.toByteArray();

            LOG.info(printInfo(record, content));
            // cleanup
            baos.reset();
        }
    }

    @Override
    public String topic() {
        return topic;
    }

    /**
     * Get header's value and convert it to integer.
     * @param headers record headers
     * @param headerKey header key
     * @return header value as integer
     */
    private int resolveInt(final Headers headers, final String headerKey) {
        Iterator<Header> iterator = headers.headers(headerKey).iterator();
        if (iterator.hasNext()) {
            byte[] value = iterator.next().value();
            return value != null ? convert(value) : INVALID;
        }
        return INVALID;
    }

    /**
     * Covert byte array to int.
     * @param value raw value
     * @return int value
     */
    private int convert(final byte[] value) {
        return ByteBuffer.wrap(value).getInt();
    }
}
