package com.msvyryd.kafka.consumer.service;

import com.msvyryd.kafka.consumer.service.handler.RecordConsumerHandler;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.msvyryd.kafka.common.CommonUtils.getEnv;
import static com.msvyryd.kafka.common.KafkaEnvVars.CONSUMER_GROUP_ID;
import static com.msvyryd.kafka.common.KafkaEnvVars.KAFKA_SERVERS;

public class KafkaRecordConsumer {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaRecordConsumer.class);
    private static final int MAX_WAIT_TIME_MS = 30_000;

    private KafkaConsumer<String, byte[]> kafkaConsumer;
    private RecordConsumerHandler<byte[]> recordConsumerHandler;

    public KafkaRecordConsumer(final RecordConsumerHandler<byte[]> recordConsumerHandler) {
        this.recordConsumerHandler = recordConsumerHandler;
        // consumer configs
        Map<String, Object> configs = new HashMap<>();
        configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, getEnv(KAFKA_SERVERS));
        configs.put(ConsumerConfig.GROUP_ID_CONFIG, getEnv(CONSUMER_GROUP_ID));
        configs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        configs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);

        // create consumer
        kafkaConsumer = new KafkaConsumer<>(configs);

        // subscribe consumer to topic
        String topic = recordConsumerHandler.topic();
        kafkaConsumer.subscribe(Collections.singletonList(topic));
    }

    public void consumeRecords() {
        long timestamp = System.currentTimeMillis();
        // define duration. 0 is allowed.
        Duration duration = Duration.ofMillis(100);
        while (timestamp + MAX_WAIT_TIME_MS >= System.currentTimeMillis()) {
            // pull records
            ConsumerRecords<String, byte[]> records = kafkaConsumer.poll(duration);

            // check if any records are returned
            if (records.isEmpty()) {
                // no records found
                continue;
            }

            // process received records
            for (ConsumerRecord<String, byte[]> record : records) {
                recordConsumerHandler.consume(record);
                LOG.info("Consumer record {}", record.key());
            }

            // update timestamp
            timestamp = System.currentTimeMillis();
        }
    }
}
