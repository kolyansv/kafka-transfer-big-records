package com.msvyryd.kafka.consumer.service.handler;

import com.msvyryd.kafka.common.KafkaEnvVars;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.msvyryd.kafka.common.CommonUtils.getEnv;
import static com.msvyryd.kafka.common.CommonUtils.printInfo;

public class PlainRecordConsumerHandler implements RecordConsumerHandler<byte[]> {
    private static final Logger LOG = LoggerFactory.getLogger(PlainRecordConsumerHandler.class);

    private String topic;

    public PlainRecordConsumerHandler() {
        this.topic = getEnv(KafkaEnvVars.TOPIC_PLAIN);
    }

    @Override
    public void consume(final ConsumerRecord<String, byte[]> record) {
        // prepare info
        String info = printInfo(record);

        // print info
        LOG.info(info);
    }

    @Override
    public String topic() {
        return topic;
    }
}
