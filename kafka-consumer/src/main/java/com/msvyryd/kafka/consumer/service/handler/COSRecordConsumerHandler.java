package com.msvyryd.kafka.consumer.service.handler;

import com.msvyryd.kafka.common.KafkaEnvVars;
import com.msvyryd.kafka.cos.COSService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

import static com.msvyryd.kafka.common.CommonUtils.*;

public class COSRecordConsumerHandler implements RecordConsumerHandler<byte[]> {
    private static final Logger LOG = LoggerFactory.getLogger(COSRecordConsumerHandler.class);

    private COSService cosService;
    private String topic;

    public COSRecordConsumerHandler(COSService cosService) {
        this.cosService = cosService;
        this.topic = getEnv(KafkaEnvVars.TOPIC_COS);
    }

    @Override
    public void consume(ConsumerRecord<String, byte[]> record) {
        // get cos key
        String cosKey = resolveCosKey(record.headers());
        if (cosKey == null) {
            LOG.warn("Unable to find {}", COS_KEY);
            return;
        }

        // download object
        String filepath = cosService.download(cosKey);
        LOG.info(printInfo(record));
        LOG.info("Downloaded file: {}", filepath);
    }

    @Override
    public String topic() {
        return topic;
    }

    /**
     * Get cos-key if exists or null.
     * @param headers record headers
     * @return cos key if exists or null
     */
    private String resolveCosKey(final Headers headers) {
        Iterator<Header> cosKeyHeaders = headers.headers(COS_KEY).iterator();
        if (cosKeyHeaders.hasNext()) {
            Header header = cosKeyHeaders.next();
            byte[] value = header.value();
            return value != null ? new String(value) : null;
        }

        return null;
    }
}
