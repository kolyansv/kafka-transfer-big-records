package com.msvyryd.kafka.consumer.service.handler;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface RecordConsumerHandler<T> {

    /**
     * Handle consumer record.
     * @param record {@link ConsumerRecord}
     */
    void consume(final ConsumerRecord<String, T> record);

    /**
     * Get topic.
     * @return kafka topic
     */
    String topic();
}
