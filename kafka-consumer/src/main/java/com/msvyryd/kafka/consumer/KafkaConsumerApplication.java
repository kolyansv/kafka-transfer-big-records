package com.msvyryd.kafka.consumer;

import com.msvyryd.kafka.common.RecordType;
import com.msvyryd.kafka.consumer.service.KafkaRecordConsumer;
import com.msvyryd.kafka.consumer.service.handler.COSRecordConsumerHandler;
import com.msvyryd.kafka.consumer.service.handler.ChunksRecordConsumerHandler;
import com.msvyryd.kafka.consumer.service.handler.PlainRecordConsumerHandler;
import com.msvyryd.kafka.consumer.service.handler.RecordConsumerHandler;
import com.msvyryd.kafka.cos.COSService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaConsumerApplication {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaConsumerApplication.class);

    public static void main(String[] args) {
        // validate input
        if (args.length != 1) {
            LOG.error("Usage:\n1st - record type");
            return;
        }

        // resolve handler type
        RecordType recordType = RecordType.resolve(args[0]);

        // create COS service
        COSService cosService = new COSService();

        // resolve handler
        RecordConsumerHandler<byte[]> recordConsumerHandler = resolveHandler(recordType, cosService);

        // create consumer service
        KafkaRecordConsumer consumer = new KafkaRecordConsumer(recordConsumerHandler);
        consumer.consumeRecords();
    }

    /**
     * Resolve record handler.
     * @param recordType {@link RecordType}
     * @param cosService {@link COSService}
     * @return {@link RecordConsumerHandler}
     */
    private static RecordConsumerHandler<byte[]> resolveHandler(final RecordType recordType, final COSService cosService) {
        switch (recordType) {
            default:
            case SIMPLE:
                return new PlainRecordConsumerHandler();
            case CHUNKS:
                return new ChunksRecordConsumerHandler();
            case COS:
                return new COSRecordConsumerHandler(cosService);
        }
    }
}
