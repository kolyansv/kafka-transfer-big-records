package com.msvyryd.kafka.producer.service.handler;

import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.List;

@FunctionalInterface
public interface RecordProducerHandler<T> {

    int DEFAULT_PARTITION = 0;

    /**
     * Produce record(s) to send.
     * @return {@link ProducerRecord}
     */
    List<ProducerRecord<String, T>> produce();
}
