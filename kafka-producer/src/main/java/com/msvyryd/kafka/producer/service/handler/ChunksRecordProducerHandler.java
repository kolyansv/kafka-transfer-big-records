package com.msvyryd.kafka.producer.service.handler;

import com.msvyryd.kafka.common.KafkaEnvVars;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.msvyryd.kafka.common.CommonUtils.*;

public class ChunksRecordProducerHandler implements RecordProducerHandler<byte[]> {
    private static final Logger LOG = LoggerFactory.getLogger(ChunksRecordProducerHandler.class);
    private static final String PREFIX = "-record-chunks-";
    private static final int CHUNK_SIZE = 100 * 1024; // 100 Kb.

    private String fileWithRecordToSend;
    private String topic;

    public ChunksRecordProducerHandler(final String fileWithRecordToSend) {
        this.fileWithRecordToSend = fileWithRecordToSend;
        this.topic = getEnv(KafkaEnvVars.TOPIC_CHUNKS);
    }

    @Override
    public List<ProducerRecord<String, byte[]>> produce() {
        long timestamp = System.currentTimeMillis();
        byte[] value = getContent(fileWithRecordToSend);

        // get total chunks
        int totalChunks = (value.length / CHUNK_SIZE) + ((value.length % CHUNK_SIZE == 0) ? 0 : 1);
        LOG.debug("Chunks number: {}", totalChunks);

        List<ProducerRecord<String, byte[]>> records = new ArrayList<>();
        for (int i = 0; i < totalChunks; i++) {
            int startIndex = CHUNK_SIZE * i;
            byte[] chunk ;
            if (i != totalChunks - 1) {
                chunk = new byte[CHUNK_SIZE];
                System.arraycopy(value, startIndex, chunk, 0, CHUNK_SIZE);
            } else {
                chunk = new byte[value.length - startIndex];
                System.arraycopy(value, startIndex, chunk, 0, value.length - startIndex);
            }
            String key = "key" + PREFIX + i + "-" + timestamp;
            ProducerRecord<String, byte[]> record = new ProducerRecord<>(
                    topic,
                    DEFAULT_PARTITION,
                    key,
                    chunk,
                    Stream.of(
                            new RecordHeader(TOTAL_CHUNKS, convert(totalChunks)),
                            new RecordHeader(CURRENT_CHUNK, convert(i + 1))
                    ).collect(Collectors.toList()));
            records.add(record);
        }

        return records;
    }

    private byte[] convert(final int value) {
        return ByteBuffer.allocate(8).putInt(value).array();
    }
}
