package com.msvyryd.kafka.producer.service;

import com.msvyryd.kafka.common.KafkaEnvVars;
import com.msvyryd.kafka.producer.service.handler.RecordProducerHandler;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static com.msvyryd.kafka.common.CommonUtils.getEnv;

public class KafkaRecordProducer {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaRecordProducer.class);
    private RecordProducerHandler<byte[]> recordProducerHandler;
    private KafkaProducer<String, byte[]> kafkaProducer;
    private Callback kafkaCallback;

    public KafkaRecordProducer(RecordProducerHandler<byte[]> recordProducerHandler) {
        this.recordProducerHandler = recordProducerHandler;

        // configs
        Map<String, Object> configs = new HashMap<>();
        configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, getEnv(KafkaEnvVars.KAFKA_SERVERS));
        configs.put(ProducerConfig.CLIENT_ID_CONFIG, getEnv(KafkaEnvVars.PRODUCER_ID));
        configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);

        kafkaProducer = new KafkaProducer<>(configs);
        kafkaCallback = new SendRecordCallback();
    }

    public void produceRecords(final int count) {
        assert count > 0;

        for (int i = 0; i < count; i++) {
            List<ProducerRecord<String, byte[]>> records = recordProducerHandler.produce();
            for (ProducerRecord<String, byte[]> record : records) {
                Future<RecordMetadata> send = kafkaProducer.send(record, kafkaCallback);
                try {
                    RecordMetadata rmd = send.get();
                    LOG.info("{} | {} | {}", rmd.topic(), rmd.partition(), rmd.offset());
                } catch (InterruptedException | ExecutionException e) {
                    LOG.warn("Error occurred", e);
                }
                LOG.info("Send {} to topic {}", record.key(), record.topic());
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // nothing to do
            }
        }
    }
}
