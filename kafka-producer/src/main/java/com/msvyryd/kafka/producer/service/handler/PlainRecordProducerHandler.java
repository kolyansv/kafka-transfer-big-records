package com.msvyryd.kafka.producer.service.handler;

import com.msvyryd.kafka.common.KafkaEnvVars;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Collections;
import java.util.List;

import static com.msvyryd.kafka.common.CommonUtils.getContent;
import static com.msvyryd.kafka.common.CommonUtils.getEnv;

public class PlainRecordProducerHandler implements RecordProducerHandler<byte[]> {
    private static final String PREFIX = "-record-plain-";

    private String fileWithRecordToSend;
    private String topic;

    public PlainRecordProducerHandler(final String fileWithRecordToSend) {
        this.fileWithRecordToSend = fileWithRecordToSend;
        this.topic = getEnv(KafkaEnvVars.TOPIC_PLAIN);
    }

    @Override
    public List<ProducerRecord<String, byte[]>> produce() {
        long timestamp = System.currentTimeMillis();
        byte[] value = getContent(fileWithRecordToSend);
        return Collections.singletonList(new ProducerRecord<>(
                topic,
                DEFAULT_PARTITION,
                "key" + PREFIX + timestamp,
                value));
    }
}
