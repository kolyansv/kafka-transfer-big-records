package com.msvyryd.kafka.producer.service.handler;

import com.msvyryd.kafka.common.KafkaEnvVars;
import com.msvyryd.kafka.cos.COSService;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.msvyryd.kafka.common.CommonUtils.COS_KEY;
import static com.msvyryd.kafka.common.CommonUtils.getEnv;

public class COSRecordProducerHandler implements RecordProducerHandler<byte[]> {
    private static final Logger LOG = LoggerFactory.getLogger(COSRecordProducerHandler.class);
    private static final String PREFIX = "-record-cos-";

    private String fileWithRecordToSend;
    private COSService cosService;
    private String topic;

    public COSRecordProducerHandler(final String fileWithRecordToSend, final COSService cosService) {
        this.fileWithRecordToSend = fileWithRecordToSend;
        this.cosService = cosService;
        this.topic = getEnv(KafkaEnvVars.TOPIC_COS);
    }

    @Override
    public List<ProducerRecord<String, byte[]>> produce() {
        long timestamp = System.currentTimeMillis();
        String key = "key" + PREFIX + timestamp;

        // upload
        String uploadedFileUrl = cosService.upload(key, new File(fileWithRecordToSend));
        LOG.info("Uploaded file URL {}", uploadedFileUrl);

        return Collections.singletonList(new ProducerRecord<>(
                topic,
                DEFAULT_PARTITION,
                key,
                new byte[0],
                Stream.of(new RecordHeader(COS_KEY, key.getBytes())).collect(Collectors.toList())));
    }
}
