package com.msvyryd.kafka.producer.service;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendRecordCallback implements Callback {
    private static final Logger LOG = LoggerFactory.getLogger(SendRecordCallback.class);

    @Override
    public void onCompletion(RecordMetadata metadata, Exception exception) {
        if (exception == null) {
            LOG.info("successfully send to topic: {} with offset: {}", metadata.topic(), metadata.offset());
        } else {
            LOG.warn("failed to send to topic {}:  with offset: {}", metadata.topic(), metadata.offset(), exception);
        }
    }
}
