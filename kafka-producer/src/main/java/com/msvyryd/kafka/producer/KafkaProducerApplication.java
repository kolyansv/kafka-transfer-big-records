package com.msvyryd.kafka.producer;

import com.msvyryd.kafka.common.RecordType;
import com.msvyryd.kafka.cos.COSService;
import com.msvyryd.kafka.producer.service.KafkaRecordProducer;
import com.msvyryd.kafka.producer.service.handler.COSRecordProducerHandler;
import com.msvyryd.kafka.producer.service.handler.ChunksRecordProducerHandler;
import com.msvyryd.kafka.producer.service.handler.PlainRecordProducerHandler;
import com.msvyryd.kafka.producer.service.handler.RecordProducerHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * KAFKA_SERVERS=localhost:9092
 * PRODUCER_ID=producer-1
 * TOPIC_PLAIN=topic-plain
 * TOPIC_CHUNKS=topic-chunks
 * TOPIC_COS=topic-cos
 *
 * CLOUD_ACCESS_KEY=
 * CLOUD_ACCESS_SECRET=
 * CLOUD_ENDPOINT=
 * CLOUD_REGION=
 * CLOUD_BUCKET=
 * LOCAL_STORAGE=
 */
public class KafkaProducerApplication {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaProducerApplication.class);

    public static void main(String[] args) {
        // validate input
        if (args.length != 3) {
            LOG.error("Usage:\n" +
                    "1st - record type\n" +
                    "2nd - records to generate\n" +
                    "3rd - file with record to send\n");
            return;
        }

        // resolve handler type
        RecordType recordType = RecordType.resolve(args[0]);
        // number of records to produce
        int count = Integer.parseInt(args[1]);

        // create COS service
        COSService cosService = new COSService();

        // resolve handler
        RecordProducerHandler<byte[]> recordConsumerHandler = resolveHandler(recordType, cosService, args[2]);

        // create consumer service
        KafkaRecordProducer producer = new KafkaRecordProducer(recordConsumerHandler);

        // send records
        producer.produceRecords(count);
    }

    /**
     * Resolve record handler.
     * @param recordType {@link RecordType}
     * @param cosService {@link COSService}
     * @return {@link RecordProducerHandler}
     */
    private static RecordProducerHandler<byte[]> resolveHandler(
            final RecordType recordType,
            final COSService cosService,
            final String fileWithRecordToSend) {
        switch (recordType) {
            default:
            case SIMPLE:
                return new PlainRecordProducerHandler(fileWithRecordToSend);
            case CHUNKS:
                return new ChunksRecordProducerHandler(fileWithRecordToSend);
            case COS:
                return new COSRecordProducerHandler(fileWithRecordToSend, cosService);
        }
    }
}
