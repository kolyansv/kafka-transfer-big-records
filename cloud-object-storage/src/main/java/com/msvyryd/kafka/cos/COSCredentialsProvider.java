package com.msvyryd.kafka.cos;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.util.StringUtils;

public class COSCredentialsProvider implements AWSCredentialsProvider {

    private String cloudAccessKey;
    private String cloudSecretKey;

    public COSCredentialsProvider(String cloudAccessKey, String cloudSecretKey) {
        this.cloudAccessKey = cloudAccessKey;
        this.cloudSecretKey = cloudSecretKey;
    }

    @Override
    public AWSCredentials getCredentials() {
        if (StringUtils.isNullOrEmpty(cloudAccessKey) || StringUtils.isNullOrEmpty(cloudSecretKey)) {
            throw new SdkClientException("access and/or secret keys are null or empty!");
        }
        return new BasicAWSCredentials(cloudAccessKey, cloudSecretKey);
    }

    @Override
    public void refresh() {
        // do nothing
    }
}
