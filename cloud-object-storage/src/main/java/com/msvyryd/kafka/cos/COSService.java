package com.msvyryd.kafka.cos;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;

import static com.msvyryd.kafka.common.CommonUtils.getEnv;
import static com.msvyryd.kafka.common.KafkaEnvVars.*;

public class COSService {
    private static final Logger LOG = LoggerFactory.getLogger(COSService.class);
    private AmazonS3 amazonS3;

    public COSService() {
        // 1. Credentials provider
        AWSCredentialsProvider credentialsProvider = new COSCredentialsProvider(
                getEnv(CLOUD_ACCESS_KEY),
                getEnv(CLOUD_ACCESS_SECRET));

        // 2. Amazon S3 client
        this.amazonS3 = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
                        getEnv(CLOUD_ENDPOINT),
                        getEnv(CLOUD_REGION)))
                .withCredentials(credentialsProvider)
                .build();
    }

    public String download(final String key) {
        // create Transfer Manager
        TransferManager transferManager = TransferManagerBuilder
                .standard()
                .withS3Client(amazonS3)
                .build();

        // construct object request
        GetObjectRequest objectRequest = new GetObjectRequest(getEnv(CLOUD_BUCKET), key);

        // file to download data to
        File cosDataFile = new File(getEnv(LOCAL_STORAGE), key);

        // download object
        Download download = transferManager.download(objectRequest, cosDataFile);

        // wait for download completion
        try {
            download.waitForCompletion();
        } catch (InterruptedException e) {
            System.err.println("Unable to download data");
            e.printStackTrace();
            return null;
        } finally {
            transferManager.shutdownNow(false);
        }

        LOG.info("Object {} successfully downloaded", key);
        // path to downloaded file
        return cosDataFile.getAbsolutePath();
    }

    public String upload(final String key, final byte[] content) {
        // construct object request
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(content.length);
        PutObjectRequest objectRequest = new PutObjectRequest(
                getEnv(CLOUD_BUCKET),
                key,
                new ByteArrayInputStream(content), metadata);

        return upload(key, objectRequest);
    }

    public String upload(final String key, final File file) {
        // construct object request
        PutObjectRequest objectRequest = new PutObjectRequest(
                getEnv(CLOUD_BUCKET),
                key,
                file);

        return upload(key, objectRequest);
    }

    private String upload(final String key, final PutObjectRequest objectRequest) {
        // create Transfer Manager
        TransferManager transferManager = TransferManagerBuilder
                .standard()
                .withS3Client(amazonS3)
                .build();

        // upload object
        Upload upload = transferManager.upload(objectRequest);

        // wait for upload completion
        try {
            upload.waitForCompletion();
        } catch (InterruptedException e) {
            System.err.println("Unable to upload data");
            e.printStackTrace();
            return null;
        } finally {
            transferManager.shutdownNow(false);
        }

        LOG.info("Object {} successfully uploaded", key);
        // FIXME
        return getEnv(CLOUD_ENDPOINT) + "/" + getEnv(CLOUD_BUCKET) + "/" + key;
    }
}
