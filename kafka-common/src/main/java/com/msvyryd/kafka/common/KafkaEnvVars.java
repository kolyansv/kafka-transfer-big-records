package com.msvyryd.kafka.common;

public enum KafkaEnvVars {
    CLOUD_ACCESS_KEY,
    CLOUD_ACCESS_SECRET,
    CLOUD_ENDPOINT,
    CLOUD_REGION,
    CLOUD_BUCKET,
    LOCAL_STORAGE,
    KAFKA_SERVERS,
    CONSUMER_GROUP_ID,
    PRODUCER_ID,
    TOPIC_PLAIN,
    TOPIC_CHUNKS,
    TOPIC_COS;
}
