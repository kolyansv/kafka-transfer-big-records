package com.msvyryd.kafka.common;

public enum RecordType {
    SIMPLE,
    CHUNKS,
    COS;

    /**
     * Resolve {@link RecordType} based on input.
     * @param type input type
     * @return {@link RecordType}
     */
    public static RecordType resolve(final String type) {
        if (type == null) {
            return SIMPLE;
        }

        for (RecordType rt : values()) {
            if (rt.name().equals(type.toUpperCase())) {
                return rt;
            }
        }

        return SIMPLE;
    }
}
