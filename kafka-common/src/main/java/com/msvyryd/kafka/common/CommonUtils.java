package com.msvyryd.kafka.common;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CommonUtils {
    public static final String TOTAL_CHUNKS = "total-chunks";
    public static final String CURRENT_CHUNK = "current-chunk";
    public static final String COS_KEY = "cos-key";

    public static byte[] getContent(final String fileWithData) {
        StringBuilder builder = new StringBuilder();
        try (FileReader fileReader = new FileReader(fileWithData);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append('\n');
            }

            if (builder.length() > 0) {
                builder.deleteCharAt(builder.length() - 1);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }
        return builder.toString().getBytes();
    }

    public static String printInfo(final ConsumerRecord<String, byte[]> record) {
        return printInfo(record, record.value());
    }

    public static String printInfo(final ConsumerRecord<String, byte[]> record, final byte[] value) {
        String key = record.key();
        Headers headers = record.headers();

        // prepare info
        StringBuilder builder = new StringBuilder();
        builder.append("---> Record details <---").append('\n')
                .append("topic: ").append(record.topic()).append('\n')
                .append("partition: ").append(record.partition()).append('\n')
                .append("offset: ").append(record.offset()).append('\n')
                .append("key: ").append(key).append('\n')
                .append("value: ").append(value != null ? new String(value) : new String(record.value())).append('\n');
        headers.forEach(header -> builder
                .append("header key: ").append(header.key()).append(" | ")
                .append("header value: ").append(new String(header.value())).append('\n'));

        return builder.toString();
    }

    public static String getEnv(final KafkaEnvVars kafkaEnvVars) {
        return System.getenv(kafkaEnvVars.name());
    }
}
