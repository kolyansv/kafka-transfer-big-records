#!/bin/bash

source .env

cd "${KAFKA_HOME}/bin" || exit

# start zookeeper
bash zookeeper-server-start.sh ../config/zookeeper.properties &
sleep 5

# start kafka
bash kafka-server-start.sh ../config/server.properties &
sleep 5
