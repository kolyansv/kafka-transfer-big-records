#!/bin/bash

source .env

cd "${KAFKA_HOME}/bin" || exit

# stop kafka
bash kafka-server-stop.sh ../config/server.properties &
sleep 5

# stop zookeeper
bash zookeeper-server-stop.sh ../config/zookeeper.properties &
sleep 5
