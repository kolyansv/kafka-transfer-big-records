#!/bin/bash

source .env

java \
-Dlogback.configurationFile="${KAFKA_PRODUCER_SERVICE}"/classes/logback.xml \
-cp "${KAFKA_PRODUCER_SERVICE}"/dependency/*:"${KAFKA_PRODUCER_SERVICE}"/kafka-producer-0.0.1.jar \
com.msvyryd.kafka.producer.KafkaProducerApplication "$@"
