#!/bin/bash

source .env

java \
-Dlogback.configurationFile="${KAFKA_CONSUMER_SERVICE}"/classes/logback.xml \
-cp "${KAFKA_CONSUMER_SERVICE}"/dependency/*:"${KAFKA_CONSUMER_SERVICE}"/kafka-consumer-0.0.1.jar \
com.msvyryd.kafka.consumer.KafkaConsumerApplication "$@"
